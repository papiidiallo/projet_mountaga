<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
	<title>Gestion de Produits Pharmaceutiques</title>
</head>
<body>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="icon-bar">
  <a href="index.php"><i class="fa fa-home">Accueil</i></a>
  <a class="active" href="médicament.php"><i class="fa fa-plus">Nouveau médicament</i></a>
  <a href="A propos.php"><i class="fa fa-globe"></i>A propos</a>
</div>

  <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Rechercher par nom...">

<ul id="myUL">
  <li><a href="#">Cetamyl</a></li>
  <li><a href="#">Epheralgan 500mg</a></li>

  <li><a href="#">Dolyprane</a></li>
  <li><a href="#">Aspegic</a></li>

  <li><a href="#">Litacold</a></li>
  <li><a href="#">Panadole</a></li>
  <li><a href="#">Ibuprofene</a></li>
</ul>
<script>
function myFunction() {
  // Declare variables
  var input, filter, ul, li, a, i, txtValue;
  input = document.getElementById('myInput');
  filter = input.value.toUpperCase();
  ul = document.getElementById("myUL");
  li = ul.getElementsByTagName('li');

  // Loop through all list items, and hide those who don't match the search query
  for (i = 0; i < li.length; i++) {
    a = li[i].getElementsByTagName("a")[0];
    txtValue = a.textContent || a.innerText;
    if (txtValue.indexOf(filter) > -1) {
      li[i].style.display = "";
    } else {
      li[i].style.display = "none";
    }
  }
}
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
</body>

</html>