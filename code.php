<?php

session_start();

try {
    $db = new PDO('mysql:host=localhost;dbname=pharmacie;charset=utf8', 'root', '');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (\Exception $e) {
    echo 'Erreur : ' . $e->getMessage();
}

if(isset($_POST['ajoutProduit'])){
    
    if(isset($_POST['nom'])) {
        $nom = $_POST['nom'];
    }
    if(isset($_POST['prix'])) {
        $prix = $_POST['prix'];
    }
    if(isset($_POST['dosage'])) {
        $dosage = $_POST['dosage'];
    }

    $req = $db->prepare("INSERT INTO produits (nom, prix, dosage) VALUES (?, ?, ?)");
    $req->execute(array($nom, $prix, $dosage));

    if($req) {

        $_SESSION['success'] = 'Produit ajouté avec succès';
        header("Location: medicament.php");
    }else {

        $_SESSION['echec'] = 'Echec de l\'ajout';
        header("Location: medicament.php");
    }

    $req->closeCursor();

}

?>