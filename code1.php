<?php

session_start();

try {
    $db = new PDO('mysql:host=localhost;dbname=pharmacie;charset=utf8', 'root', '');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (\Exception $e) {
    echo 'Erreur : ' . $e->getMessage();
}


if(isset($_GET['id'])) {

    $id = $_GET['id'];
    
    $req = $db->prepare("DELETE FROM produits WHERE id = ?");
    $req->execute(array($id));

    if($req) {

        $_SESSION['success'] = 'Produit supprimé avec succès';
        header("Location: index.php");
    }else {

        $_SESSION['echec'] = 'Echec de suppression';
        header("Location: index.php");
    }

    $req->closeCursor();


    
}

?>

