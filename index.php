<?php
session_start();
include_once 'Layouts/header.php';
// include_once 'Layouts/navbar.php';

?>

<div class="icon-bar">
  <a class="active" href="index.php"><i class="fa fa-home">Accueil</i></a>
  <a href="medicament.php"><i class="fa fa-plus">Médicament</i></a>
  <a href="A propos.php"><i class="fa fa-globe">A propos</i></a>
  
</div>

<div class="container-fluid">
	<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
  <ol class="carousel-indicators">
    <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <!-- <img src="images/u.jpg" class="d-block w-100" alt="..."> -->
      <div class="carousel-caption d-none d-md-block">
      </div>
    </div>
  </a>
</div>

<?php

try {
  $db = new PDO('mysql:host=localhost;dbname=pharmacie;charset=utf8', 'root', '');
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (\Exception $e) {
  echo 'Erreur : ' . $e->getMessage();
}

$req = $db->query("SELECT * FROM produits");

?>

<div class="container">
  <div class="card mt-4 mb-4">
    <div class="card-header">
      <h3 class="text-success">Liste des médicaments</h3>
    </div>
    <div class="card-body">
    <?php

if(isset($_SESSION['success']) && $_SESSION['success'] != ""){
    echo '<p style="text-align: center;" class="alert alert-success display-hide"><strong>Succes ! </strong> ' . $_SESSION['success'] . ' </p>';
        unset($_SESSION['success']);
}
if(isset($_SESSION['echec']) && $_SESSION['echec'] != ""){
    echo '<p style="text-align: center;" class="alert alert-danger display-hide"><strong>Erreur ! </strong> ' . $_SESSION['status'] . ' </p>';
        unset($_SESSION['status']);
}
?>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Nom</th>
            <th>Prix</th>
            <th>Dosage</th>
            <th>Date d'enregistrement</th>
            <th>Supprimer un médicament</th>
          </tr>
        </thead>
        <tbody>
          <?php
            while($donnees = $req->fetch()) {
              ?>
          <tr>
            <td></td>
            <td><?php echo $donnees['nom']; ?></td>
            <td><?php echo $donnees['prix']; ?> FCFA</td>
            <td><?php echo $donnees['dosage']; ?> mg</td>
            <td><?php echo $donnees['date_creation']; ?></td>
            <td>
              <a href="code1.php?id=<?php echo $donnees['id']; ?>"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
          <?php
            }
            $req->closeCursor();
            ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<?php

include_once 'Layouts/footer.php';
include_once 'Layouts/script.php';

?>
