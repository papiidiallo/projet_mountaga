<?php
session_start();
include_once 'Layouts/header.php';
// include_once 'Layouts/navbar.php';

?>

<div class="icon-bar">
  <a href="index.php"><i class="fa fa-home">Accueil</i></a>
  <a class="active" href="medicament.php"><i class="fa fa-plus">Médicament</i></a>
  <a href="A propos.php"><i class="fa fa-globe">A propos</i></a>
  
</div>



<div class="container ">
   <form action="code.php" method="POST">
    <div class="card mt-4 mb-4">
            <div class="card-header">
                <h2 class="text-success">Nouveau Médicament</h2>
            </div>
            <div class="card-body">
            <?php

            if(isset($_SESSION['success']) && $_SESSION['success'] != ""){
                echo '<p style="text-align: center;" class="alert alert-success display-hide"><strong>Succes ! </strong> ' . $_SESSION['success'] . ' </p>';
                    unset($_SESSION['success']);
            }
            if(isset($_SESSION['echec']) && $_SESSION['echec'] != ""){
                echo '<p style="text-align: center;" class="alert alert-danger display-hide"><strong>Erreur ! </strong> ' . $_SESSION['status'] . ' </p>';
                    unset($_SESSION['status']);
            }
            ?>

                <div class="form-group mb-3">
                    <label for="">Nom du produit</label>
                    <input type="text" name="nom" class="form-control">
                </div>
                <div class="form-group mb-3">
                    <label for="">Prix du produit</label>
                    <input type="number" name="prix" class="form-control">
                </div>
                <div class="form-group mb-3">
                    <label for="">Dosage du produit</label>
                    <input type="number" name="dosage" class="form-control">
                </div>
                <div class="form-group mb-3">
                    <button type="submit" name="ajoutProduit" class="btn btn-outline-success">Valider</button>
                    <button type="reset" class="btn btn-outline-danger">Annuler</button>
                </div>
            </div>
        </div>
   </form>
</div>

<?php

include_once 'Layouts/footer.php';
include_once 'Layouts/script.php';

?>